package com.andrewcode.bitvalue;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.Point;
import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Handler;
import android.text.Html;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity {
    private Context context;
    private static String url = "https://bitpay.com/api/rates";

    private static final String Vcode = "code";
    private static final String Vrate = "rate";
    private static final String Vname = "name";

    private boolean debug = false;
    private boolean debug1 = false;

    int refreshRate = 30000;

    TextView bitView;
    String state;
    String dollars;

    private int width;
    private int height;

    String saveDollar = "0";
    String symbol = "&#36;";

    private boolean resumeHasRun = false;

    ArrayList<String> jsonlist = new ArrayList<String>();

    Timer myTimer = new Timer();
    MyTimerTask myTimerTask= new MyTimerTask();

    private Menu menu;

    int CurrencyIndex = 0;
    int DollarIndex = 1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //new ProgressTask(MainActivity.this).execute();

        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        width = size.x;
        height = size.y;

        myTimer.scheduleAtFixedRate(myTimerTask, 0, refreshRate);
        bitView = (TextView)findViewById(R.id.value);

        bitView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                //Log.e("Yo i was clicked", "Fo a reaaal long time.");
                final CharSequence[] items = {"US Dollar (USD)","Eurozone Euro (EUR)",
                        "Pound Sterling (GBP)","Japanese Yen (JPY)",
                        "Canadian Dollar (CAD)","Australian Dollar (AUD)","Chinese Yuan (CNY)"
                        ,"Swiss Franc (CHF)","Swedish Krona (SEK)",
                        "New Zealand Dollar (NZD)"," South Korean Won (KRW)","UAE Dirham (AED)"};

                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                builder.setTitle("Select Currency");
                builder.setItems(items, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int item) {
                        Toast toast = Toast.makeText(getApplicationContext(), items[item]+" Selected", 2000);
                        toast.setGravity(Gravity.CENTER_VERTICAL, 0, height - 20);
                        toast.show();
                        switch (item){
                            case 0:     CurrencyIndex = 0;
                                        DollarIndex = 1;
                                        symbol = "&#36;";
                                        new ProgressTask(MainActivity.this).execute();
                                break;
                            case 1:     CurrencyIndex = 3;
                                        DollarIndex = 4;
                                        symbol = "&#8364;";
                                        new ProgressTask(MainActivity.this).execute();
//                                        stopRefresh();
                                break;
                            case 2:     CurrencyIndex = 6;
                                        DollarIndex = 7;
                                        symbol = "&#163;";
                                        new ProgressTask(MainActivity.this).execute();
                                        break;
                            case 3:     CurrencyIndex = 9;
                                        DollarIndex = 10;
                                        symbol = "&#165;";
                                        new ProgressTask(MainActivity.this).execute();
                                        break;
                            case 4:     CurrencyIndex = 12;
                                        DollarIndex = 13;
                                        symbol = "&#36;";
                                        new ProgressTask(MainActivity.this).execute();

                                        break;
                            case 5:     CurrencyIndex = 15;
                                        DollarIndex = 16;
                                        symbol = "&#36;";
                                        new ProgressTask(MainActivity.this).execute();
                                break;
                            case 6:     CurrencyIndex = 18;
                                        DollarIndex = 19;
                                        symbol = "&#165;";
                                        new ProgressTask(MainActivity.this).execute();
                                break;
                            case 7:     CurrencyIndex = 21;
                                        DollarIndex = 22;
                                        symbol = "&#8355;";
                                        new ProgressTask(MainActivity.this).execute();
                                break;
                            case 8:     CurrencyIndex = 24;
                                        DollarIndex = 25;
                                        symbol = "";
                                        new ProgressTask(MainActivity.this).execute();
                                break;
                            case 9:     CurrencyIndex = 27;
                                        DollarIndex = 28;
                                        symbol = "&#36;";
                                        new ProgressTask(MainActivity.this).execute();
                                break;
                            case 10:     CurrencyIndex = 30;
                                         DollarIndex = 31;
                                         symbol = "&#8361;";
                                         new ProgressTask(MainActivity.this).execute();
                                break;
                            case 11:     CurrencyIndex = 33;
                                         DollarIndex = 34;
                                            symbol = "";
                                         new ProgressTask(MainActivity.this).execute();
                                break;
                        }
                    }
                }).show();

                return false;
            }
        });

    }

    private class ProgressTask extends AsyncTask<String, Void, Boolean> {
        private ProgressDialog dialog;

        private Activity activity;

        public ProgressTask(Activity activity) {
            this.activity = activity;
            context = activity;
            dialog = new ProgressDialog(context);
        }

        private Context context;

        protected void onPreExecute() {
        }

        @Override
        protected void onPostExecute(final Boolean success) {
            state = jsonlist.get(CurrencyIndex);
            dollars = jsonlist.get(DollarIndex);
            bitView = (TextView)findViewById(R.id.value);

            if(debug){
                Log.e("on Pre Execute","State = "+state+"dollars = "+ dollars);
            }

            if(Double.parseDouble(saveDollar) == 0){
                DecimalFormat df = new DecimalFormat("#,###.##");
                String tempDollars = df.format(Double.parseDouble(dollars));
                String temp = "1<br>Bitcoin<br>"+"=<br>"+state+"<br>"+symbol+tempDollars;
                bitView.setText(Html.fromHtml(temp));
                if(debug){
                    Log.e("Got value", "init");
                }
            }

            if(Double.parseDouble(saveDollar)!= 0 && Double.parseDouble(saveDollar) < Double.parseDouble(dollars)){
                DecimalFormat df = new DecimalFormat("#,###.##");
                String tempDollars = df.format(Double.parseDouble(dollars));
                String high = "1<br>Bitcoin<br>"+"=<br>"+state+"<br>"+"<font color=#588F27>"+symbol+tempDollars+"</font>";
                bitView.setText(Html.fromHtml(high));
                if(debug){
                    Log.e("Got value", "high");
                }
            }//640.53

            if(Double.parseDouble(saveDollar)!= 0 && Double.parseDouble(saveDollar) > Double.parseDouble(dollars)){
                DecimalFormat df = new DecimalFormat("#,###.##");
                String tempDollars = df.format(Double.parseDouble(dollars));
                String low = "1<br>Bitcoin<br>"+"=<br>"+state+"<br>"+"<font color=#B9121B>"+symbol+tempDollars+"</font>";
                bitView.setText(Html.fromHtml(low));
                if(debug){
                    Log.e("Got value", "low");
                }
            }
            saveDollar = dollars;
            jsonlist.clear();

            Toast toasty = Toast.makeText(getApplicationContext(), "BitValue Refreshed", 500);
            toasty.setGravity(Gravity.CENTER_VERTICAL, 0, height - 20);
            toasty.show();

        }

        protected Boolean doInBackground(final String... args) {

            JSONParser jParser = new JSONParser();

            // get JSON data from URL
            JSONArray json = jParser.getJSONFromUrl(url);

            for (int i = 0; i < json.length(); i++) {

                try {

                    JSONObject c = json.getJSONObject(i);

                    String vcode = c.getString(Vcode);

                    Double vrate = c.getDouble(Vrate);
                    String vname = c.getString(Vname);


                    jsonlist.add(vcode);

                    DecimalFormat df = new DecimalFormat("#.##");
                    jsonlist.add(String.valueOf(df.format(vrate)));
                    jsonlist.add(vname);
                    if(debug1){
                        Log.e("AsyncTask", ""+jsonlist.toString());
                    }

                }
                catch (JSONException e) {
                    e.printStackTrace();
                    Log.e("JSONException", "Sorry, something went very wrong.");
                }
            }
            return null;
        }
    }

    private class MyTimerTask extends TimerTask {
        @Override
        public void run() {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    new ProgressTask(MainActivity.this).execute();
                    if(debug){
                        Log.e("MyTimerTask", "Run the timer");
                    }
                }
            });
        }
    }

    @Override
    public void onPause(){
        super.onPause();
        myTimer.cancel();

        if(debug){
            Log.e("ON pause", "This is on pause");
        }
    }

    @Override
    public void onRestart(){
        super.onRestart();
        Timer myTimer = new Timer();
        MyTimerTask myTimerTask= new MyTimerTask();
        myTimer.scheduleAtFixedRate(myTimerTask, 0, refreshRate);
        if(debug){
            Log.e("ON restart", "This is restart");
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!resumeHasRun) {
            resumeHasRun = true;
            return;
        }
        Timer myTimer = new Timer();
        MyTimerTask myTimerTask= new MyTimerTask();
        myTimer.scheduleAtFixedRate(myTimerTask, 0, refreshRate);
        if(debug){
            Log.e("ON resume", "This is resume");
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        new MenuInflater(this).inflate(R.menu.main, menu);
        return (super.onCreateOptionsMenu(menu));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        final MenuItem i = item;

        switch (item.getItemId()) {
            case R.id.refresh_option:
                new ProgressTask(MainActivity.this).execute();
                refreshIcon(item);
                Handler h = new Handler();

                h.postDelayed(new Runnable() {

                    @Override
                    public void run() {
                        stopRefresh(i);
                    }
                }, 750);

                return (true);
        }
        return super.onOptionsItemSelected(item);
    }



    private void refreshIcon(MenuItem item){
        LayoutInflater inflater = (LayoutInflater) getApplication()
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        ImageView iv = (ImageView) inflater.inflate(R.layout.action_refresh,
                null);

        Animation rotation = AnimationUtils.loadAnimation(getApplication(),
                R.anim.refresh_rotate);
        rotation.setRepeatCount(Animation.INFINITE);
        iv.startAnimation(rotation);

        item.setActionView(iv);
    }

    private void stopRefresh(MenuItem item){
        item.getActionView().clearAnimation();
        item.setActionView(null);
    }
}